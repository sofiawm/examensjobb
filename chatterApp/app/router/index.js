'use strict'
const u = require('../utils');
const passport = require('passport');
const config = require('../config');

module.exports = () => {
    
    let routes = {
        
        'get': {
            '/': (res) => {
                res.render('login')
        },

            '/rooms': [u.isAuthenticated, (req, res) => {
                    res.render('rooms', {
                        user: req.user,
                        host: config.host
                    });
        }],
            '/chat/:id': [u.isAuthenticated, (req, res, next) => {
                //TODO: see if this works, dont know that iam doing 
                let getRoom = u.findById(req.app.locals.chatrooms, req.params.id)
                // IF room dont match, undefined
                if(getRoom === undefined){ 
                    return next();
                // ELSE room match, render
                } else {
                    res.render('chatroom', {
                        user: req.user,
                        host: config.host,
                        room: getRoom.room,
                        roomID: getRoom.roomID
                    });

                }

        }],
           '/auth/facebook': passport.authenticate('facebook'),
           '/auth/facebook/callback': passport.authenticate('facebook', {
               successRedirect: '/rooms',
               failureRedirect: '/'
           }),
           '/auth/local': passport.authenticate('local'),
			'/auth/local/callback': passport.authenticate('local', {
				successRedirect: '/rooms',
				failureRedirect: '/'
			}),
			'/auth/google': passport.authenticate('google'),
			'/auth/google/callback': passport.authenticate('google', {
				successRedirect: '/rooms',
				failureRedirect: '/'
			}),

           '/logout': (req, res) => {
               req.logout();
               res.redirect('/');
               
           }

           
        /*    '/getSession': (req, res, next) => {
                    res.send("test to see that session works as expected", req.session.message)
        },
            '/setSession': (req, res, next) => {
                req.session.message = "Hello World";
                req.send("Session set")

        },*/
        
    },
        'post': {

            '/': (req, res, next) => {
                res.render('/rooms')
            } 
        },
        '404': {
            '/': (req, res, next) => {
                res.status('404').sendFile(process.cwd() + '/views/404.htm');
            }
        }
    }
    
    return u.route(routes);

}