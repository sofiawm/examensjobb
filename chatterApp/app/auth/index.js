'use strict'
const passport = require('passport')
const config = require('../config')
const u = require('../utils')
const logger = require('../logger');
const facebookStrategy = require('passport-facebook').Strategy;
const googleStrategy = require('passport-google-oauth').OAuth2Strategy;
const localStrategy  = require("passport-local")

module.exports = () => {
    passport.serializeUser((user, done ) => {
        done(null, user.id);

    });

    passport.deserializeUser((id, done) => {
        u.findById(id)
            .then(user => done(null, user))
            .catch(error => logger.error('error','error when deserilizing user', + error));
    });

    let authProcessor = (accessToken, refreshToken, profile, done) => {
         // find user in db with profile.id
         // if user is find, return data
         // if user not find, create in db 
        u.findUser(profile.id)
            .then(result => {
                if(result){
                    done(null, result);
              } else {
                  // create new user
                u.createNewUser(profile)
                    .then(newChatUser => done(null, newChatUser))
                    .catch(error => logger.error('error','error when creating new user' + error))
                }
            });
    }
    
    
    passport.use(new facebookStrategy(config.fb, authProcessor));
    passport.use(new googleStrategy(config.go, authProcessor));
    passport.use(new localStrategy(config.local, authProcessor));
}