'use strict' 

const config = require('../config')
const logger = require('../logger');
const Mongoose = require('mongoose').connect(config.dbURI)

// error if connection fails
Mongoose.connection.on('error', error => {
// check to see if works
    logger.log('error','mongoDB error: ' + error);
});
// schema structure, storing users
const chatUser = new Mongoose.Schema({
    profileId: String,
    fullName: String,
    profilepic: String
});


let userModul = Mongoose.model('chatUser', chatUser);

module.exports = {
    Mongoose, 
    userModul

}
