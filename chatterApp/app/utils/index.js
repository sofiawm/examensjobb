'use strict'

const router = require('express').Router
const db = require('../db');
const crypto = require('crypto');
/*const socket = require('socket.io')*/

// "loop tru" routes
let _registerRoutes = (routes, method) => {
    for(let key in routes) 
    if(typeof routes[key] === 'object' && router[key] !== null && !(routes[key] instanceof Array)){
        _registerRoutes(routes[key], key);
    } else {
        if(method === 'get') {
            router.get(key, router[key]);
        } else if(method === 'post') {
            router.post(key, router[key]); }
            else {
                router.use(routes[key])
            }
        }
    }

let route = routes => {
__registerRoutes(routes);
return router;
}

// find use based on id/key
let findUser = profileID => {
    return db.userModul.findUser({
        'profileId': profileID
    });

}

// crete new user
let createNewUser = profile => {
    return new Promise((resolve, reject) => {
        let newChatUser = new db.userModul({
            profileId: profile.id, 
            fullName: profile.displayName,
            profilePic: profile.photos[0].value || ''

        });
         
        newChatUser.save(error => {
            if(error) {
                console.log('Create new user error')
                reject(error)
            } else {
                resolve(createNewUser)
            }
        });
    });
}

// find user based on id
let findById =  id => {
    return new Promise((resolve, reject => {
        db.userModul.findById(id, (error, user) => {
            if(error){
                reject(error);  
          } else {
                resolve(user);
            }
        });
    }));
}

// check if user is authentication
let isAuthenticated = (res, req, next) => {
    if(req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/');
    }
}

// find chatroom based on name
let findRoomByName = (allrooms, room) => {
    let findRoom = allrooms.findIndex((element, index, array) => {
        if(element.room === room){
            return true;
        } else {
            return false;
        }
    });
// Hopefully makes sure that rooms only are created ones, BAD solution
// TODO, if time: create better solution. regular expression etc?
return findRoom > -1 ? true : false;
}

// renders a unique room id
let randomHex = () => {
    return crypto.randomBytes(24).toString('hex')

}

// find chatoom based on id 
let findRoomById = (allrooms, roomID ) => {
    return allrooms.find((element, index, array) => {
        if(element.roomID === roomID) {
            return true;
        } else {
            return false;
        }   

    });
}

// add user to room
let addChatUserToRoom = (allrooms, data, socket) => {
    //console.log(data.roomID)
    let getChatRoom = findRoomById(allrooms, data.roomID)
    if(getChatRoom !== undefined) {
        //get user
        let chatUserID = socket.request.session.passport.user;
        //check if user existx in chatroom
        let checkUser = getChatRoom.users.findIndex((element) => {
            if(element.chatUserID === chatUserID){
                return true;
            } else {
                return false;  
                
            }
        });
        //remove if already exist, find better solution if time 
        if(checkUser > -1) {
            getChatRoom.users.splice(checkUser, 1);

        }
        //push user into room hopefully
        //fråga tony om detta ens är korrekt, funkar ej?!
        getChatRoom.users.push({
            socketID: socket.id,
            chatUserID,
            user: data.user, 
            userPic: data.userPic
        

        });

        socket.join(data.roomID)
        
        // return room
        return getChatRoom;
    }
}

// find and remove user
let removeUserFromChatRoom = (allrooms, socket) => {
    for(let disRoom of allrooms){
        //find user
        let findUser = disRoom.users.findIndex((element) => {
            if(element.socketID === socket.id){
                return true; 
            } else {
                return false;
            }

        // if element.socketID === socket.id ? true : false
        });
        if(findUser > -1)
                socket.leave(disRoom.roomID)
                disRoom.users.splice(findUser, 1);
                return disRoom;

    }
}




module.exports = {
    route,
    findUser,
    createNewUser, 
    findById,
    isAuthenticated, 
    findRoomByName,
    randomHex, 
    findRoomById,
    addChatUserToRoom, 
    removeUserFromChatRoom

}