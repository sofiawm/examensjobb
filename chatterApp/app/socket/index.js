'use strict'
const u = require('../utils');

module.exports = (io, app) => {

    let allRooms = app.locals.chatrooms;
    // see if allRooms.push works as expected
    // TODO: change from hardcoded to dynamic
    /* allRooms.push({
        rooms: 'Mat',
        roomsID: '0001',
        users: []

    });

    allRooms.push({
        rooms:'sports',
        rommsID: '0002',
        users: []
    })
*/
    io.of('/roomslist').on('connection', socket => {
        socket.on('getChatrooms', () => {
            socket.emit('chatroomsList', JSON.stringify(allRooms))
        });
        //console.log('socket.io connected to client?')
        socker.on('createNewRoom', newRoomInput => {
            //console.log(newRoomInput)
            // check to see if room with title exist
            // if no, create and broadcast
            if(!u.findRoomByName(allRooms, newRoomInput)){
                allRooms.push({
                    rooms: newRoomInput,
                    roomID: u.randomHex(),
                    users: []
                });
                // only sends updated roomslist to user
                socket.emit('chatRoomsList', JSON.stringify(allRooms));
                //hopefully sends it to all users, not sure about broadcast tho?
                socket.broadcast.emit('chatRoomsList', JSON.stringify(allRooms));
            }
        });
    });

    io.of('/chatter').on('connection', socket => {
        //join chatroom

            socket.on('join', data => {
                // console.log(data);
                let userList = u.addChatUserToRoom(allRooms, data, socket)
                // update list for all 
                socket.broadcast.to(data, roomID).emit('updateUserLst', JSON.stringify(userList.users))
                //console.log('userList', userList)
                //update list for user
                socket.emit('updateUserList', JSON.stringify(userList.users))
            });

            // socket exist
        socket.on('disconnect', () => {

            // find that room, add user
            let disRoom = u.removeUserFromChatRoom(allRooms, socket);
            socket.broadcast.to(disRoom.roomID).emit('uppdateUserList', JSON.stringify(disRoom.users))

        });
        // when (new) msg ...
        socket.on('newMessage', data => {
            socket.to(data.roomID).emit('message', JSON.stringify(data))

        });

    });

}