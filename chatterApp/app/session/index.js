'use strict'

const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const config = require('../config');
const db = require('../db')

if(process.env.NODE_ENV === 'production') {
    // session for prod
    module.exports = session({
        secret: config.sessonSecret,
        resave: false,
        saveUninitialized: false,
        store: new MongoStore({
            mongooseConnection: db.Mongoose.connection
        })
    });
} else {
    // sessions for dev
    module.exports = session({
        secret: config.sessonSecret,
        resave: false,
        saveUninitialized: true
        
    });
}


