'use strict'

const config = require('./config');
const redis = require('redis').createClient;
const socketRedis = require('socket.io-redis');

// social auth 
require('/auth')();

/*const router = require('express').Router();

app.get('/', (req, res, next) => {
    res.render('login');
});

app.get('/info', (req, res, next) => {
    res.send('page to test router');
})*/

// create  io server
let serverIo = app => {
    app.locals.chatrooms = [];
    const server = require('http').Server(app);
    const io = require('socket.io')(server);
    io.set('transports', ['websocket']);
    let pubClient = redis(config.redis.port, config.redis.host, {
        auth_pass: config.redis.password
    });
    let subClient = redis(config.redis.port, config.redis.host, {
        auth_pass: config.redis.password
    });

    io.adapter(socketRedis({
        pubclient, 
        subClient
    }));
    io.use((socket, next) => {
        require('./session')(socket.request, {}, next);
    });
    require('./socket')(io, app)
    return server;

}

module.exports = {
    router: require('./router'),
    sessions: require('./session'),
    serverIo,
    io
}