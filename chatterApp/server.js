'use strict'

const express = require('express');
const app = express();
const chatterApp = require('./app');
const passport = require('passport');

app.set('port', process.env.PORT || 3000)

/*let routerMiddle = (req, res, next) => {
    req.hello = "hello world, dashboard";
    next(); 
}

app.use('/', routerMiddle)*/
app.use(express.static('public'));
app.set('view engine', 'ejs'); 
app.use(chatterApp.session);
app.use(passport.initialize());
app.use(passport.session());

app.use('/', chatterApp.router);
/*app.get('/', (req, res, next) => {
    res.send('<h1>hello world</h1>');

}); 

app.get('/dashboard', (res, reg, next) => {
    res.send('<h1> this is dashboard page</h1>')
});
*/
chatterApp.serverIo(app).listen(app.get('port'), () => {
        console.log('running on port:', app.get('port'));

});
